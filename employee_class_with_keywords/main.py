class Employee:
    def __init__(self, full_name: str, **kwargs):
        split_full_name = full_name.split(' ')
        self.name = split_full_name[0]
        self.surname = split_full_name[1]
        for key, value in kwargs.items():
            setattr(self, key, value)


john = Employee("John Doe")
richard = Employee("Richard Roe", salary=11000, height=178)
mary = Employee("Mary Major", salary=120000)
giancarlo = Employee("Giancarlo Rossi", salary=115000, height=182, nationality="Italian")
print(john.name)
print(mary.surname)
print(richard.height)
print(giancarlo.nationality)
