import math


class Pagination:
    current_page = 1

    def __init__(self, items=None, pageSize=float(10)):
        if items is None:
            items = []
        self.items = items
        self.pageSize = int(pageSize)

    def getVisibleItems(self):
        if self.current_page == 1:
            for content in range(0, self.pageSize):
                print(f'{content + 1:02}. {self.items[content]}')

        else:
            lower_range = (self.current_page - 1) * self.pageSize
            upper_range = (self.current_page * self.pageSize)

            if upper_range <= len(self.items):
                for content in range(lower_range, upper_range):
                    print(f'{content + 1:02}. {self.items[content]}')
            # when we have last page we have to watch out for IndexError, so the upper range is len of all items in page
            else:
                for content in range(lower_range, len(self.items)):
                    print(f'{content + 1:02}. {self.items[content]}')

    def prevPage(self):
        self.current_page -= 1

    def nextPage(self):
        self.current_page += 1
        # be returning itself we can chain methods
        return self

    def firstPage(self):
        self.current_page = 1

    def lastPage(self):
        # round to highest amount of page to find how match page we need to print all items
        amount_of_pages = math.ceil(len(self.items) / self.pageSize)
        self.current_page = amount_of_pages

    def goToPage(self, page):
        self.current_page = page


alphabtet = [letter for letter in "abcdefghijklmnopqrstuvwxyz"]
page = Pagination(alphabtet, 4)
page.nextPage().nextPage()
page.getVisibleItems()
