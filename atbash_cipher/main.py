import string

"""
ALGORITHM
indexes of letters:
0   -1      
1   -2      
2   -3      
3   -4 etc.
So we see that the "mirror" letter will be always opposite and absolute value will be greater by 1
We can add 1 to letter index in ascii array and then add '-' before parentheses to make the opposite index mark
"""


def atbash_cipher(text):
    result = ''
    for letter_index in range(len(text)):
        if text[letter_index] in string.ascii_lowercase:                 # for lower letters
            tmp_index = string.ascii_lowercase.find(text[letter_index])  # index of letter from user text in ascii list
            result += string.ascii_lowercase[-(tmp_index + 1)]
        elif text[letter_index] in string.ascii_uppercase:               # for upper letters
            tmp_index = string.ascii_uppercase.find(text[letter_index])
            result += string.ascii_uppercase[-(tmp_index + 1)]
        else:                                                            # for another sign
            result += text[letter_index]
    return result


if __name__ == "__main__":
    print(atbash_cipher('apple'))
    print(atbash_cipher('Hello world!'))
    print(atbash_cipher('Christmas is the 25th of December!'))
