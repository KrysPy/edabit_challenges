import re


# function make all occurring prefixes or suffixes
def make_list_of_pre_or_suffix(dire, choice):
    pattern = re.compile(r'(\w+)\.([a-z]+)')
    final_list = []
    for file in dire:
        match = pattern.finditer(file)
        for i in match:
            if i.group(choice) not in final_list:
                final_list.append(i.group(choice))
    return final_list


def sort_by_prefix(prefix_list, dire):
    result_list = []
    for prefix in prefix_list:
        tmp_lst = [file for file in dire if file.startswith(prefix)]        # temporary list for specific prefix
        result_list.append(tmp_lst)                                         # then we adding tmp_list to final list
    return result_list


def sort_by_suffix(suffix_list, dire):
    result_list = []
    for suffix in suffix_list:
        tmp_lst = [file for file in dire if file.endswith(suffix)]
        result_list.append(tmp_lst)
    return result_list


if __name__ == '__main__':
    # if looking by PREFIX send to function choice=1
    # if looking by SUFFIX send to function choice=2
    # TESTS
    dire1 = ["ex1.html", "ex1.js", "ex2.html", "ex2.js"]    # by prefix
    prefix_list = make_list_of_pre_or_suffix(dire1, 1)
    print(sort_by_prefix(prefix_list, dire1))
    dire2 = ["music_app.js", "music_app.png", "music_app.wav", "tetris.png", "tetris.js"]
    prefix_list = make_list_of_pre_or_suffix(dire2, 1)
    print(sort_by_prefix(prefix_list, dire2))
    dire3 = ["ex1.html", "ex1.js", "ex2.html", "ex2.js"]
    suffix_list = make_list_of_pre_or_suffix(dire3, 2)
    print(sort_by_suffix(suffix_list, dire3))
    dire4 = ["_1.rb", "_2.rb", "_3.rb", "_4.rb"]
    suffix_list = make_list_of_pre_or_suffix(dire4, 2)
    print(sort_by_suffix(suffix_list, dire4))
    dire5 = ["_1.rb", "_2.rb", "_3.rb", "_4.rb"]
    prefix_list = make_list_of_pre_or_suffix(dire5, 1)
    print(sort_by_prefix(prefix_list, dire5))
