from collections import Counter


def advanced_sort_list(lst):
    counted_elements = Counter(lst)
    sorted_list = []
    for i in counted_elements.keys():
        sublist = [i for _ in range(counted_elements.get(i))]
        sorted_list.append(sublist)
    return sorted_list


lst1 = [3, 2, 4, 1, 4, "b", "a", 2, 1, 3]
lst2 = [5, 4, 5, 5, 4, 3]
lst3 = ["b", "a", "b", "a", "c"]
print(advanced_sort_list(lst1))
print(advanced_sort_list(lst2))
print(advanced_sort_list(lst3))
