import datetime


def sort_dates(dates: list, mode: str):
    if mode == "ASC":
        reverse = False
    else:
        reverse = True
    # converting from string to datatime object
    data_objects = [datetime.datetime.strptime(date, '%d-%m-%Y_%H:%M') for date in dates]
    # sort list with specific flag
    data_objects.sort(reverse=reverse)
    # convert list with sorted datatime objects to list with strings
    result_list = [datetime.datetime.strftime(date, '%d-%m-%Y_%H:%M') for date in data_objects]
    return result_list


print(sort_dates(["10-02-2018_12:30", "10-02-2016_12:30", "10-02-2018_12:15"], "ASC"))
print(sort_dates(["10-02-2018_12:30", "10-02-2016_12:30", "10-02-2018_12:15"], "DSC"))
print(sort_dates(["09-02-2000_10:03", "10-02-2000_18:29", "01-01-1999_00:55"], "ASC"))
