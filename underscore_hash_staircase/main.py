def staircase(n):
    underscore_counter = 0
    result = ''
    if n < 0:
        for i in range(n, 0, 1):
            result += underscore_counter * '_'
            result += (abs(i) * '#') + '\n'
            underscore_counter += 1
    elif n > 0:
        for i in range(1, n + 1):
            result += (n - i) * '_'
            result += i * '#' + '\n'
    return result



print(staircase(2))