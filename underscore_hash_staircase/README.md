# Underscore-Hash Staircase

Create a function that will build a staircase using the underscore _ and hash # symbols. 
A positive value denotes the staircase's upward direction and downwards for a negative value.



Notes:
- All inputs are either positive or negative values.
- The string to be returned is adjoined with the newline character (\n).
- A recursive version of this challenge can be found here.