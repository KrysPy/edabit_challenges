import re

txt1 = "Exception 0xAF"
txt2 = "Exception 0xD3"
txt3 = "Exception 0xd3"
txt4 = "Exception 0xZZ"

pattern = re.compile(r'x[A-F0-9][A-F0-9]')

print(re.findall(pattern, txt1))
print(re.findall(pattern, txt2))
print(re.findall(pattern, txt3))
print(re.findall(pattern, txt4))
