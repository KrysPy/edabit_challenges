import re

txt = "... <!-- My -- comment test --> ..  <!----> .. "
pattern = re.compile(r'<!--.*?-->')

print(re.findall(pattern, txt))
