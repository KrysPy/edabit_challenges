import re

txt1 = "red flag blue flag"
txt2 = "yellow flag red flag blue flag green flag"
txt3 = "pink flag red flag black flag blue flag green flag red flag"
pattern = re.compile(r'(red|blue) flag')

new1 = pattern.finditer(txt3)
for match in new1:
    print(match)
