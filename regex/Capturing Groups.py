import re

txt = "01:32:54:67:89:AB"
pattern = re.compile(r'^(?:[0-9a-fA-F]:?){12}$')
MAC = re.findall(pattern, txt)
print(bool(len(MAC) == 6))
print(MAC)
