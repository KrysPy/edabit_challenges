import re

pattern = re.compile(r'\Bend\B')
print(bool(re.search(pattern, "The end of the story.")))
print(bool(re.search(pattern, "Endings are pointless.")))
print(bool(re.search(pattern, "Let's send!")))
print(bool(re.search(pattern, "We viewed the rendering at the end.")))
print(bool(re.search(pattern, "Sometimes bending the rules is good.")))
