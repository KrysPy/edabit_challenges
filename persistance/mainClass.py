# function splitting gave number and make addition or multiplication
def spliting_int_and_checking(number, sign):
    sum = 0
    splitedNumber = [part for part in str(number)]
    if sign == '+':
        for number in splitedNumber:
            sum += int(number)
    elif sign == "*":
        sum = 1         # because multiplication has to be per 1
        for number in splitedNumber:
            sum *= int(number)
    return sum


def additive_or_multiplitive_persistance(number, sign):
    tmp = spliting_int_and_checking(number, sign)     # temporary variable
    integerCounter = 0
    if 0 <= number <= 9:
        return integerCounter
    integerCounter += 1
    while tmp >= 10:
        integerCounter += 1
        tmp = spliting_int_and_checking(tmp, sign)
    return integerCounter
