def group_parentheses(string: str):
    list_of_groups = []
    end_of_group = 0                         # place in sting when one cluster of parenthesis is end and start next
    while (end_of_group + 1) != len(string):
        left_counter = 0                     # counting left parenthesis
        right_counter = 0
        tmp = ''
        for i in range(end_of_group, len(string)):
            if string[i] == "(":
                left_counter += 1
                tmp += "("
            else:
                right_counter += 1
                tmp += ")"
            if left_counter == right_counter:
                list_of_groups.append(tmp)
                end_of_group = i
                tmp = ''
    return list_of_groups


print(group_parentheses("((()))(())()()(()())"))
