import numpy as np


def id_matrix(n):
    if not isinstance(n, int):
        raise ValueError("Value should be int")
    else:
        if n > 0:
            array = np.identity(n)
        elif n < 0:
            array = np.identity(-n)
            array = array[::-1]
        else:
            array = []
    return array


if __name__ == "__main__": 
    print(id_matrix(2))
    print(id_matrix(-2))
    print(id_matrix(0))
    
