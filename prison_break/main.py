def freed_prisoners(cells):
    free_prisoners_counter = 0
    counter = 0
    while counter < len(cells):
        if cells[counter] == 1:
            free_prisoners_counter += 1
            cells = [i ^ 1 for i in cells]
        counter += 1
    return free_prisoners_counter


cells = [1, 1, 0, 0, 1, 0]
cells2 = [1, 1, 1]
cells3 = [0, 0, 0]
cells4 = [0, 1, 1, 1]
print(freed_prisoners(cells4))
