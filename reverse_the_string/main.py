# won't work with duble space between words
def special_reverse_string(string: str):
    result = ''
    letter_index = 0        # index of current adding letter from string without spaces

    # we have to find indexes od lower, upper cases and spaces, also when we have digit in string we adding this like
    # uppercase letters to get their indexes
    uppercase_indexes = [i for i in range(len(string)) if string[i].isupper() or string[i].isdigit()]
    lowercase_indexes = [i for i in range(len(string)) if string[i].islower()]
    space_indexes = [i for i in range(len(string)) if string[i].isspace()]

    # now we reverse the primary string and remove all spaces
    string = string[::-1].lower().replace(" ", "")
    for i in range(len(string) + len(space_indexes)):
        if i in uppercase_indexes:
            result += string[letter_index].upper()
            letter_index += 1
        elif i in lowercase_indexes:
            result += string[letter_index].lower()
            letter_index += 1
    # when we adding a space we have to remember about letter which was in this place before in reversed string
        elif i in space_indexes:
            result += " "
    return result


print(special_reverse_string("1 23 456"))
