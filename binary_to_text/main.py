def binary_to_decimal(number: str):
    result = 0
    elements = [i for i in number]
    elements = elements[::-1]
    for i in range(len(elements)):
        result += (int(elements[i]) * (2 ** i))
    return result


def binary_to_text(number: str):
    result = ''
    elements = [number[i: i + 8] for i in range(0, len(number), 8)]
    to_decimal = [binary_to_decimal(element) for element in elements]
    for element in to_decimal:
        result += chr(element)
    return result


print(binary_to_text("011100000111100101110100011010000110111101101110"))
