Write a function that returns the smallest integer in a list with its corresponding index and its parity. 
Even though this challenge can be achieved easily with the modulo operator %, index() and min() methods, these have been restricted to augment the challenge's level of difficulty.

Output Structure

{"@index " + index_of_smallest: smallest_integer, "parity": "odd|even"}

Examples

bitwise_one_zero([107, 19, -18, -79, 36, 23, 97]) ➞ {"@index 3": -79, "parity": "odd"}

bitwise_one_zero([31, 7, 2, 13, 7, 9, 10, 13]) ➞ {"@index 2": 2, "parity": "even"}

bitwise_one_zero([3, 3, 3, 3, 3, 3]) ➞ {"@index 0": 3, "parity": "odd"}

Notes
- The set of limitations imposed on this challenge dictates the level of difficulty.
- Another version of this challenge that deals with recursion can be found here.