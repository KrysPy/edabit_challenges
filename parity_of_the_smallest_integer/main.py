def bitwise_one_zero(elements: list):
    current_the_smallest_num = elements[0]
    index_of_the_smallest_num = 0
    parity = 'Even'
    odd_ends = ('1', '3', '5', '7', '9')

    for index in range(len(elements)):
        if elements[index] < current_the_smallest_num:
            current_the_smallest_num = elements[index]
            index_of_the_smallest_num = index

    for suffix in odd_ends:
        if str(current_the_smallest_num).endswith(suffix):
            parity = 'Odd'
    return current_the_smallest_num, index_of_the_smallest_num, parity


current_the_smallest_num, index_of_the_smallest_num, parity = bitwise_one_zero([3, 3, 3, 3, 3, 3])
print(f'@index {index_of_the_smallest_num}: {current_the_smallest_num}, parity: {parity}')
