def simplify(fraction: str):
    numbers = fraction.split('/')
    # nominator is smaller than denominator
    if int(numbers[1]) > int(numbers[0]):
        dividers = []
        for divider in range(2, int(numbers[1])):
            if int(numbers[0]) % divider == 0 and int(numbers[1]) % divider == 0:
                dividers.append(divider)
        # when we cannot simplified fractions
        if len(dividers) == 0:
            result = fraction
        # when we can simplified fractions
        else:
            nominator = round(int(numbers[0]) / max(dividers))
            denominator = round(int(numbers[1]) / max(dividers))
            result = f"{nominator}/{denominator}"
    else:
        result = int(numbers[0]) / int(numbers[1])
        # when we get int after simplified
        if result % 2 == 0:
            result = str(round(result))
        else:
            integer = round(int(numbers[0]) / int(numbers[1]))
            rest = int(numbers[0]) % int(numbers[1])
            result = f"{integer} ane {rest}/{numbers[1]}"
    return result


print(simplify('37/4'))
