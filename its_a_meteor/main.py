import re

def will_hit(trajectory: str, coordinates: tuple):
    re_pattern = re.compile(r'-?[0-9.]+')
    factors = re.findall(re_pattern, trajectory)
        
    # the shorest possibility: 'y = x + 2' => len == 9
    if len(trajectory) < 9:
        raise Exception("Wrong format: y = mx + b (b value is required)")
    
    # b value cannot be equal 0
    if trajectory[-1] == '0':
        raise Exception("b value cannot be equal 0!")
    
    # when e.g.: 'y = -x + 3'
    if len(factors) == 1:
        # if the m value is 1, the "1" will be shown, same situatuin with -1 
        m = -1 if trajectory[4] == '-' else 1
        b = int(factors[0])
        print(f"y = {m}x + {b}")
    else:
        # m value will always be an integer
        try:
            m = int(factors[0])
            b = int(factors[1])
        except ValueError:
            print("m and b values have to be integers!")
            return
                
    x, y = coordinates
    
    right_side = m * x + b
    
    if y == right_side:
        return True
    else:
        return False

if __name__ == "__main__":
    print(will_hit("y = 2x - 5", (0, 0)))
    print()
    print(will_hit("y = -4x + 6", (1, 2)))
    print()
    print(will_hit("y = 2x + 6", (3, 2)))
    print()
    print(will_hit("y = x + 6", (3, 2)))
    print()
    print(will_hit("y = -x + 6", (3, 2)))
    print()
    print(will_hit("y = 2x + 0", (3, 2)))
    print()
    