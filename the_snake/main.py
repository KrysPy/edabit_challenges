def snakefill(side_tiles: int):
    snake_length = 1
    eats = 0
    
    while True:
        snake_length *= 2
        
        if snake_length > side_tiles**2 :
            break
        
        eats += 1
        
    return eats

if __name__ == "__main__":
    print(snakefill(3))
    print(snakefill(6))
    print(snakefill(18))
    print(snakefill(24))
    print(snakefill(100))
