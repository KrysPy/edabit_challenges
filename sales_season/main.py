"""ALGORITHM

DISCOUNT_PERCENT = ((ALL_PRICES - FREE_ITEMS_PRICE) * 100%) / FULL_PRICE_OF_ALL_ITEMS

"""


# return list of prices after discount and message
def discount(list_of_prices: list):
    full_discount = 0
    amount_of_free_products = len(list_of_prices) // 3

    # no discount case
    if amount_of_free_products == 0:
        return list_of_prices, 'No discount applied'

    sorted_prices = sorted(list_of_prices)

    # counting full discount
    for i in range(amount_of_free_products):
        full_discount += sorted_prices[i]

    # counting full price after discount
    full_price_after_disc = round(sum(list_of_prices) - full_discount, 2)

    # counting percent discount
    percent_discount = round(((full_price_after_disc * 100) / sum(list_of_prices)), 2)

    prices_after_discount = [round(price * (percent_discount / 100), 2)
                             for price in list_of_prices]

    result_message = f"{amount_of_free_products} discounts applied"
    return prices_after_discount, result_message


list_after_discount, message = discount([68.74, 68.74, 17.85, 55.99, 10.75, 11.68, 74.56])
print(list_after_discount)
print(message)
