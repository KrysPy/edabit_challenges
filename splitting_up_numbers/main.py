splitted = []


def num_split(num: int):
    num = str(num)

    if num[0] == '-':
        num = num[1:]
        sign = -1
    else:
        sign = 1

    rev_num = num[::-1]
    multiplier = 1

    for i in rev_num:
        to_add = sign * (int(i) * multiplier)
        multiplier *= 10
        splitted.append(to_add)

    return splitted[::-1]


print(num_split(-575456768347))
