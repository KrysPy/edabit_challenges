import datetime


def bed_time(time_and_alarm: list):
    result_list = []
    for element in time_and_alarm:
        # alarm time
        hours1, minutes1 = transform_to_numbers(element[0])
        alarm = datetime.datetime(1, 1, 1, hour=hours1, minute=minutes1)
        # duration
        hours2, minutes2 = transform_to_numbers(element[1])
        duration = datetime.datetime(1, 1, 1, hour=hours2, minute=minutes2)
        result = str(alarm - duration)
        # -8:-3  --- cause that is just HH:MM in returning string and that'll be constant range
        result_list.append(result[-8:-3])
    return result_list


def transform_to_numbers(time: str):
    if time[0] == '0':              # when 07:--
        hours = int(time[1])
    else:
        hours = int(time[:2])       # when 17:--
    if time[3] == '0':
        minutes = int(time[4])      # when --.05
    else:
        minutes = int(time[3:])     # when --.15
    return hours, minutes


print(bed_time([["05:45", "04:00"], ["07:10", "04:30"]]))
