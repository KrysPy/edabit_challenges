def time_adding(time: str, time_to_add: tuple):
    hours, minutes, seconds = transform_to_numbers(time)
    hours_to_add, minutes_to_add, seconds_to_add = time_to_add
    # seconds
    tmp = (seconds + seconds_to_add) // 60
    final_sec_result = round((seconds + seconds_to_add) % 60)
    # minutes
    minutes_to_add += tmp
    tmp = (minutes_to_add + minutes) // 60
    final_min_result = round((minutes + minutes_to_add) % 60)
    # hours
    hours_to_add += tmp
    final_hours_result = round((hours + hours_to_add) % 24)
    result = time_format((final_hours_result, final_min_result, final_sec_result))
    return result


def transform_to_numbers(time: str):
    if time[0] == '0':                  # when 07:--:--
        hours = int(time[1])
    else:
        hours = int(time[:2])           # when 17:--:--
    if time[3] == '0':
        minutes = int(time[4])          # when --:05:--
    else:
        minutes = int(time[3:5])        # when --:15:--
    if time[6] == '0':
        seconds = int(time[7])          # when --:--:05
    else:
        seconds = int(time[6:])         # when --:--:15
    return hours, minutes, seconds


def time_format(time_components: tuple):
    result = ''
    for component in time_components:
        if component <= 9:
            result += "0" + str(component) + ":"
        else:
            result += str(component) + ":"
    return result[:-1]


print(time_adding("00:00:00", (72, 120, 120)))
