import string


def sorting(sentence: str):
    result = ''
    uppercase = [letter for letter in sentence if letter.isupper()]
    lowercase = [letter for letter in sentence if letter.islower()]
    numbers = [sign for sign in sentence if sign.isdigit()]
    numbers.sort()

    for letter in string.ascii_lowercase:
        if letter in lowercase:
            result += letter
        if letter.upper() in uppercase:
            result += letter.upper()

    result += ''.join(numbers)
    return result


print(sorting("Re4r"))
