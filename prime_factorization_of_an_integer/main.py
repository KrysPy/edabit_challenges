def find_the_smallest_divider(n):
    for i in range(2, n + 1):
        if n % i == 0:
            return i


def prime_factors(n):
    result = 0
    dividers = []
    while result != 1:
        divider = find_the_smallest_divider(n)
        result = round(n / divider)
        n = result
        dividers.append(divider)
    return dividers


print(prime_factors(8912234))
