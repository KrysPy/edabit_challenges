from math import sqrt
import csv

GRAVITATIONAL_CONST = 6.674 * (10 ** -11)   # Nm^2/kg^2
EARTH_MASS = 5.976 * (10 ** 24)             # kg
EARTH_RADIUS = 6378000.00                   # m


def get_mass_and_radius(planet: str):
    with open('planet_data.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader, None)
        for row in csv_reader:
            if row[0] == planet.capitalize():
                planet_mass = float(row[1]) * EARTH_MASS
                planet_radius = float(row[2]) * EARTH_RADIUS
        return planet_mass, planet_radius


def escape_velocity_calc(planet: str):
    # getting planet mass and radius
    planet_mass, planet_radius = get_mass_and_radius(planet)
    # velocity in m/s
    escape_vel_m_s = sqrt((2 * GRAVITATIONAL_CONST * planet_mass) / planet_radius)
    escape_vel_km_h = round(escape_vel_m_s * 3.60, 2)
    escape_vel_km_s = round(escape_vel_m_s / 1000, 2)
    result = f"The escape velocity in:\n- m/s: {round(escape_vel_m_s, 2)}\n- km/h: {escape_vel_km_h}" \
             f"\n- km/s: {escape_vel_km_s}"
    return result


if __name__ == '__main__':
    print(escape_velocity_calc("Mars"))
