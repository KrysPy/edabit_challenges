import re


def equal_count(mixed_string: str, names: str):
    splited_names = names.split('&')
    counted_names = {}
    # first we count of occurrences in mixed string
    for name in splited_names:
        pattern = re.compile(name)
        found = pattern.findall(mixed_string)
        # in found variable we get the list of all occurrences current name in string so the amount of occurrences
        # is length of that list (found)
        counted_names[name] = len(found)

    # now we can check equality of TWO names
    if counted_names[splited_names[0]] == counted_names[splited_names[1]]:
        counted_names['Equality'] = True
    else:
        counted_names['Equality'] = False
        difference = abs(counted_names[splited_names[0]] - counted_names[splited_names[1]])
        counted_names['Difference'] = difference
    return counted_names


print(equal_count("Peter!@#$Paul&*#Peter!--@|#$Paul#$Peter@|Paul$%^^Peter++Paul%$%^Peter++Paul#$#$#Peter@|Paul",
                  "Peter&Paul"))
print(equal_count("Elliot!@#$Sam!--@|#$Elliot@|Sam++Elliot$%^Elliot@|Sam!@#Elliot!@#$Sam!--@|#$Elliot", "Sam&Elliot"))
print(equal_count("Tim!@#$Kit&&*#Tim!--@|#$Kit#$%Tim@|Kit$%^^Tim++Kit%$%^Tim++Kit#$#$#Tim@|Kit", "Ken&Tom"))
