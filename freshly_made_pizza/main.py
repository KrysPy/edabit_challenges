class Pizza:

    order_number = 1

    def __init__(self, ingredients: list):
        self.order_number = Pizza.order_number
        self.ingredients = ingredients
        Pizza.order_number += 1

    @classmethod
    def hawaiian(cls, ingredients=('ham', 'pineapple')):
        return cls(ingredients)

    @classmethod
    def meat_festival(cls, ingredients=('beef', 'meatball', 'bacon')):
        return cls(ingredients)

    @classmethod
    def garden_fest(cls, ingredients=('spinach', 'olives', 'mushroom')):
        return cls(ingredients)


p1 = Pizza.hawaiian()
print(p1.ingredients)


