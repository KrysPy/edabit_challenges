import string

def abacaba(n):
    result = ''
    for i in range(n):
        tmp = string.ascii_uppercase[i]
        result = result + tmp + result
    return result


print(abacaba(6))
