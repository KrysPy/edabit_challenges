The ABACABA pattern is a recursive fractal pattern that shows up in many places in the real world 
(such as in geometry, art, music, poetry, number systems, literature and higher dimensions).

Create a function that takes a number n as an argument and returns a string that represents the full pattern.

abacaba_pattern(1) ➞ "A"

abacaba_pattern(2) ➞ "ABA"

abacaba_pattern(3) ➞ "ABACABA"

Notes:
- Result should always be uppercase.