from function import can_see_stage

stage = [[1, 2, 3], # true
         [4, 5, 6],
         [7, 8, 7],
         [8, 9, 9]]

stage2 = [[1, 2, 3, 2, 1, 1],   # true
          [2, 4, 4, 3, 2, 2],
          [5, 5, 5, 10, 4, 4],
          [6, 6, 7, 6, 5, 5]]

stage3 = [[2, 0, 0],    # false
          [1, 1, 1],
          [2, 2, 2]]

stage4 = [[1, 0, 0],    # false
          [1, 1, 1],
          [2, 2, 2]]

print(can_see_stage(stage2))
