def can_see_stage(stage):
    rows = len(stage)
    columns = len(stage[0])
    # sprawdzanie dla kazdej wiersza w kazdej kolumnie, zapis aktualnego maksimum w kolumnie
    for col in range(columns):
        tmp = stage[0][col]
        for row in range(1, rows):
            if stage[row][col] > tmp:
                tmp = stage[row][col]
            else:
                return False
    return True
