# Luhn Algorithm

Create a function that takes a number and checks whethers the given 
number is a valid credit card number using Luhn Algorithm. The return value is boolean.

valid_credit_card(4111111111111111) ➞ True
# Visa Card

valid_credit_card(6451623895684318) ➞ False
# Not a valid credit card number.

valid_credit_card(6451623895684318) ➞ False


https://en.wikipedia.org/wiki/Luhn_algorithm
https://dev.to/anuragrana/python-script-validating-credit-card-number-luhn-s-algorithm-2f7c