# Also called MOD 10 ALGORITHM


def valid_credit_card(card_number):
    list_of_numbers = [int(i) for i in str(card_number)]
    list_of_numbers.reverse()
    is_index_odd = False             # first index of multiplying num is even and then change one by one (0, 1, 2, 3...)
    sum_of_multiplying_nums = 0
    print(list_of_numbers)
    for num in list_of_numbers:
        if is_index_odd:
            num *= 2
            if num > 9:
                num -= 9                        # equal to: 1 + 6 = 7 ===> 16 - 9 = 7
        sum_of_multiplying_nums += num          # equal multiply by 1 (even index of num in reversed card number)
        is_index_odd = not is_index_odd
    return sum_of_multiplying_nums % 10 == 0    # when mod 10 is equal 0 returning True


print(valid_credit_card(4111111111111111))
